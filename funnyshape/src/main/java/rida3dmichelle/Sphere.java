//Rida Chaarani

package rida3dmichelle;

public class Sphere implements Shape3d {

    private double radius;


    /**
     * Throws an exception when called
     * @exception UnsupportedOperationException
     * @author Rida Chaarani
     * 
     */
    public double getVolume() {
        return ((4 * Math.PI * Math.pow(this.radius, 3))/3);
    }

     /**
     * Throws an exception when called
     * @exception UnsupportedOperationException
     * @author Rida Chaarani
     * 
     */
    public double getSurfaceArea() {
        return (4 * Math.PI * Math.pow(this.radius, 2));
    }


    /**
     * Creates a sphere object
     * 
     * @author Rida Chaarani
     * @param radius is the radius of the sphere
     *  */    
    public Sphere(double radius) {
        this.radius = radius;
        if(radius < 0){
            throw new IllegalArgumentException("Not written yet");
        }
        
    }

    /**
     * returns the radius of the shpere
     * 
     * @return radius
     * @author Rida Chaarani
     * 
     */
    public double getRadius() {
        return this.radius;
    }
}