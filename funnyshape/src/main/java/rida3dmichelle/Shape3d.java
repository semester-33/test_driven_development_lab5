package rida3dmichelle;

public interface Shape3d {
    double getVolume();
    double getSurfaceArea();
}
