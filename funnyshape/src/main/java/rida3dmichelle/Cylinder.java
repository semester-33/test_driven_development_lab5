package rida3dmichelle;

public class Cylinder implements Shape3d{

    private double radius;
    private double height;
    
    /**
     * Creates the cylinder object
     *  
     * @param radius 
     * @param height
     * 
     * @author Uyen Dinh Michelle Banh
     * 
    */

    public Cylinder(double radius, double height){
        this.radius = radius;
        this.height = height;

        if (radius < 0 || height < 0){
            throw new IllegalArgumentException("The dimensions can't be negative!");
        }
    }

    /**
     * @return radius - will get the radius store in the field
     * 
     * @author Uyen Dinh Michelle Banh
     * 
     */

    public double getRadius(){
        return this.radius;
    }
    
    /**
     * @return height - will get the radius store in the field
     * 
     * @author Uyen Dinh Michelle Banh
     * 
     */

    public double getHeight(){
        return this.height;
    }

    /**
     * @returns the volume of the cylinder
     * 
     * @author Uyen Dinh Michelle Banh
     */

    public double getVolume(){
        return Math.PI*Math.pow(this.radius, 2)*this.height;
    }

    /**
     * @exception UnsupportedOperationException
     * 
     * @author Uyen Dinh Michelle Banh
     */

    public double getSurfaceArea(){
        return (2*Math.PI*this.radius*this.height)+(2*Math.PI*Math.pow(this.radius, 2));
    }
}
