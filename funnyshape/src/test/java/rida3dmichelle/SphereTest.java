package rida3dmichelle;
import static org.junit.Assert.*;
import org.junit.Test;

public class SphereTest {
    @Test
    public void check_return_radius(){
        Sphere s = new Sphere(3);
        assertEquals(3, s.getRadius(), 0.1);
    }
    @Test
    public void check_volume_throw_exception(){
        Sphere s = new Sphere(3);
        assertEquals(36*Math.PI, s.getVolume(), 0.0001);
    }
    @Test
    public void check_surface_throw_exception(){
        Sphere s = new Sphere(3);
        assertEquals(36*Math.PI, s.getSurfaceArea(), 0.0001);
    }
    @Test 
    public void check_negative_value_radius() {
        Sphere s = new Sphere(-3);
    }

}