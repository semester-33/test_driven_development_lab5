package rida3dmichelle;
import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {
    
    @Test
    public void constructorTestNegativeRadius() {
        Cylinder c = new Cylinder(-3.0, 13.0);
    }

    @Test
    public void constructorTestNegativeHeight() {
        Cylinder c = new Cylinder(3.0, -13.0);
    }


    @Test
    public void getRadiusTrue() {

        Cylinder c = new Cylinder(3.0, 13.0);

        assertEquals(3.0, c.getRadius(), 0.0001);
    }

    @Test
    public void getHeightTrue() {

        Cylinder c = new Cylinder(3.0, 13.0);

        assertEquals(13.0, c.getHeight(), 0.0001);
    }

    @Test
    public void getVolumeFalse() {
        Cylinder c = new Cylinder(3.0, 13.0);

        assertEquals(367.56634, c.getVolume(), 0.00001);
    }

    @Test
    public void getSurfaceFalse() {
        Cylinder c = new Cylinder(3.0, 13.0);

        assertEquals(301.59289474462014, c.getSurfaceArea(), 0.00001);
    }
}
